import axios from "axios";

export const setNumber = (number) => {
  return {
    type: "SET_NUMBER",
    payload: {
      newNumber: number,
    },
  };
};

export const changeText = (text) => {
  return {
    type: "CHANGE_TEXT",
    payload: {
      text: text,
    },
  };
};


export const fetchRandomInt = () => {
  return async (dispatch) =>{
  var response = await axios.get("/API/randomINT")
 dispatch(setNumber(response.data));//dispatch new request here with all of the data now in hand
      
  };
};

  
//combine all reducers

import numberReducer from "./number"
import textReducer from "./text"

import {combineReducers} from "redux"

const funnelReducer = combineReducers({
// name of piece of state : name of reducer
    number: numberReducer,
    text: textReducer
})
export default funnelReducer
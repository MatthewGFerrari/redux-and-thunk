// articel content reducer

const textReducer = (state = "Change Text Here", action) => {
    switch (action.type) {
         case "CHANGE_TEXT":          
            return action.payload.text;
        default:
           return state;
    }
}
export default textReducer;
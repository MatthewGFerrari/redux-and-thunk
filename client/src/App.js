import React from 'react';
import './App.css';
import {useSelector, useDispatch} from "react-redux"
import {changeText} from "./actions"
import Display from "./components/display"
import Button from "./components/button"
import Text from "./components/text"

function App() {
  const num = useSelector(state=>state.number)
  const text = useSelector(state=>state.text)
  console.log("everything")
  var send = (e) =>{
    console.log(e)
    document.getElementById("textField").value = e.target.value
    dispatch(changeText(document.getElementById("textField").value))
}
const dispatch = useDispatch()
  

  return (
    <div >
      <header >
        WELCOME TO THE RANDOM NUMBER GENERATOR! THIS APP IS UGLY BUT USES REDUX AND THUNK SO WHO CARES!
        </header>
  <p>Your current number is {num} and here is a place to change the text for the sake of it! Your text is "{text}"</p>        
  <input id = "textField" value = {text} onChange={(e)=>send(e)}/>

     
      <Display></Display>
      <Button/>
      <br></br>
<Text/>
    </div>
  );
}

export default App;

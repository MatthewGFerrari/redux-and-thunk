import React from 'react';
import {useSelector, useDispatch} from "react-redux"
import {changeText} from "../actions"


function Text() {
  const text = useSelector(state=>state.text)
const dispatch = useDispatch()
console.log("text")

var send = (e) =>{
    console.log(e)
    document.getElementById("textField").value = e.target.value
    dispatch(changeText(document.getElementById("textField").value))
}
  return (
    <p >
        
        <input id = "textField" value = {text} onChange={(e)=>send(e)}/>
        <br></br>
    </p>
  );
}
export default Text
import React from 'react';
import {useSelector, useDispatch} from "react-redux"
import {fetchRandomInt} from "../actions"

function Display() {
  const num = useSelector(state=>state.number)
const dispatch = useDispatch()
  console.log(num)

  return (
    <p >
        
        <button onClick={() => dispatch(fetchRandomInt())}>RANDOM INT</button>
        <br></br>

        {num}
    </p>
  );
}
export default Display
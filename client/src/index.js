import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import funnelReducer from "./reducers";


// to allow for asyncronous calls to APIs you must only do 2 things
//1) install thunk and pass it into the store
//2) curry the dispatch events in the actions as demonstrated in this project with the fetchRandomInt action
const store = createStore(
  funnelReducer,
  /* preloadedState, */ 
  compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : (f) => f
  )
);
ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
//REDUX FOR DUMMIES
//The Actions---- the firing pins
//Actions are functions that take ALL inputs neccesary to update state and return an object composed of the info needed to update state
//The object returned is comprised of 2 parts: Type(it's title) and the payload (the info needed to update state)

//The Reducers---- The mini states that make up the global state
//There can be different types of reducers--every type of reducer is a different sub section of state
//they are functions that recieve their previous state and the object returned by the action
//They then must take these params and return their new state
//Every action goes through all reducers; the reducer determines what to do (if anything) depending on the type (title) of the object
//If it doesnt want to do anything, it returns its initial state

//The Funnel----a combiner
//Houses all reducers and gives them the name that will be their sub section of state

//The Store---- a global state with listeners
//The store is the global state that is composed of the reducers' states
//The components subscribe to the store to have the oppertunity to ask for its information

//The Dispatchers----The notifiers
//Dispatch method is included in redux library
//The components use the  dispatch method to notify the store that something happenend and give it the info to act
// The store will pass this info to the reducers and let them determine what to do with it

//The interaction as a whole
//The component subscribes to the store using subscribe(); calling this method returns the unsubscribe() method to call on unmount
//The component dispatches an action prefilled w/ neccesary arguments using store.dispatch(setCount(42))
//The action returns an object and the component now dispatches this object to the store
//The store runs the action through the funnel
//The funnel takes the object and gives it to all of the reducers along with the reducers mini-state
//Each reducer reads the object's type (title) and determine what they want to do with it via switch-case
//If the reducer wants to take the object based on it's type (title), it uses the information in the object to return it's updated state
//If the reducer doesnt want to take the object based on it's type (title), it returns its default state
//The component is re-rendered and it calls store.getState().subsection.variable in its render method
//Compont calls unsubscribe() when it is ready to re-render
